﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace ZoobookWpfTest.ViewModel
{
	public class TimeGapViewModel : ViewModelBase
	{
		#region Properties and Fields

		private DateTime _selectedDateTime;
		private ICollection<DateTime> _inputDateTimes;
		private ObservableCollection<DateTime> _timeOutDateTimes;
		private ObservableCollection<DateTime> _timeInDateTimes;
		private DateTime _viewDate;
		private ObservableCollection<TimeSpan> _timeGapDateTimes;
		private DateTime _selectedDateTimeIn;
		private DateTime _selectedDateTimeOut;
		private ObservableCollection<string> _sortedDateTimes;
		private ObservableCollection<string> _timeGapIn;
		private ObservableCollection<string> _timeGapOut;

		public DateTime ViewDate
		{
			get => _viewDate;
			set => Set(ref _viewDate, value);
		}

		public ObservableCollection<DateTime> TimeOutDateTimes
		{
			get => _timeOutDateTimes;
			set => Set(ref _timeOutDateTimes, value);
		}

		public ObservableCollection<DateTime> TimeInDateTimes
		{
			get => _timeInDateTimes;
			set => Set(ref _timeInDateTimes, value);
		}

		public ObservableCollection<TimeSpan> TimeGapDateTimes
		{
			get => _timeGapDateTimes;
			set => Set(ref _timeGapDateTimes, value);
		}

		public ObservableCollection<string> TimeGapIn
		{
			get => _timeGapIn;
			set => Set(ref _timeGapIn, value);
		}

		public ObservableCollection<string> TimeGapOut
		{
			get => _timeGapOut;
			set => Set(ref _timeGapOut, value);
		}

		public ObservableCollection<string> SortedDateTimes
		{
			get => _sortedDateTimes;
			set => Set(ref _sortedDateTimes, value);
		}

		public DateTime SelectedDateTime
		{
			get => _selectedDateTime;
			set => Set(ref _selectedDateTime, value);
		}

		public DateTime SelectedDateTimeIn
		{
			get => _selectedDateTimeIn;
			set => Set(ref _selectedDateTimeIn, value);
		}

		public DateTime SelectedDateTimeOut
		{
			get => _selectedDateTimeOut;
			set => Set(ref _selectedDateTimeOut, value);
		}

		#endregion


		public TimeGapViewModel()
		{
			TimeInDateTimes = new ObservableCollection<DateTime>();
			TimeOutDateTimes = new ObservableCollection<DateTime>();
			TimeGapDateTimes = new ObservableCollection<TimeSpan>();
			TimeGapOut = new ObservableCollection<string>();
			TimeGapIn = new ObservableCollection<string>();

			SortedDateTimes = new ObservableCollection<string>();
			SelectedDateTime = new DateTime();
			SelectedDateTimeIn = new DateTime();
			SelectedDateTimeOut = new DateTime();
		}

		public ICommand AddTimesCommand => new RelayCommand(AddItemToList);
		public ICommand ClearTimesCommand => new RelayCommand(ClearItems);

		private void ClearItems()
		{
			TimeInDateTimes.Clear();
			TimeOutDateTimes.Clear();
		}

		public ICommand ShowTimeSpan => new RelayCommand(ShowTimeSpans);

		private void ShowTimeSpans()
		{
			TimeGapIn.Clear();
			SortedDateTimes.Clear();
			TimeGapOut.Clear();
			TimeGapDateTimes.Clear();
			if (TimeInDateTimes.Count != 0 && TimeOutDateTimes.Count != 0)
			{
				CalculateGap(TimeInDateTimes,TimeOutDateTimes);
			}
			else
			{
				MessageBox.Show("Please input valid dates", "Alert", MessageBoxButton.OK);

			}
		}

		private void AddItemToList()
		{
			var newdate = new DateTime();
			if (SelectedDateTimeIn == newdate || SelectedDateTimeOut == newdate)
			{
				MessageBox.Show("Please input a valid date", "Alert", MessageBoxButton.OK);
				return;
			}
			TimeInDateTimes.Add(SelectedDateTimeIn);
			TimeOutDateTimes.Add(SelectedDateTimeOut);
		}

		public void SortDateTimes()
		{
			var sortedDates = TimeInDateTimes.ToList().OrderBy(t => t.TimeOfDay);
			foreach (var sortedDate in sortedDates)
			{
				SortedDateTimes.Add(sortedDate.ToString("MM/dd/yyyy"));
			}
		}

		public void CalculateGap(ICollection<DateTime> timeInList, ICollection<DateTime> timeOutList)
		{
			try
			{
				if (timeInList.Count == 0 && timeOutList.Count == 0) return;
				var timeInObjs = timeInList.ToList();
				var timeOutObjs = timeOutList.ToList();
				for (var i = 0; i < timeInList.Count; i++)
				{
					var timeOut = timeOutObjs[i];
					//handler for exception
					if (i == timeInList.Count-1)
					{
						break;
					}
					
					var newTimeIn = timeInObjs[i+1];

					if (timeOut.Day != newTimeIn.Day || timeOut.Month != newTimeIn.Month) continue;
					TimeGapIn.Add(timeOut.AddMinutes(1).ToString("T"));
					SortedDateTimes.Add(newTimeIn.ToString("MM/dd/yyyy"));
					TimeGapOut.Add(newTimeIn.Add(new TimeSpan(0, 0, -1, 0)).ToString("T"));
					var timeGap = newTimeIn - timeOut;
					TimeGapDateTimes.Add(timeGap);
				}

			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}

		}


	}
}
